import { Component } from '@angular/core';
import { ServerComponent } from '../server/server.component';

@Component({
  selector: 'app-server-two',
  templateUrl: './server-two.component.html',
  styleUrls: ['./server-two.component.css'],
  styles:[`.online{
    color:white;
  }`]
})
export class ServerTwoComponent {
  isDisabled = true;
  public names:string = "10";
  public serverId =  10;
  public serverStatus = "offline";
  public serverName = "Ana";
  public isServerCreated = false;
  public servers:string[] = [];
   


  constructor(){
     Math.random()>=0.5? this.serverStatus= "online": this.serverStatus= "offline"
      setTimeout(()=>{this.isDisabled =false}, 2000);}

 
public getServerName(event:Event){
    this.isServerCreated =true;
    this.servers.push(this.names);
    this.names = "An";
    this.serverName =(<HTMLInputElement>event.target).value;
       console.log(event);}

public getColor(){
 return this.serverStatus==='online'?'green':'red';
}

}
