import { Component } from "@angular/core";
import { ServerTwoComponent } from "../server-two/server-two.component";

@Component({selector:'server-app',
templateUrl: './server.component.html',
styleUrls:['./server.component.css'],
styles:[`.online{
    color: white;
}`]
})

 export class ServerComponent extends ServerTwoComponent{
   
}